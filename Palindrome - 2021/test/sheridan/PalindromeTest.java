package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean test=Palindrome.isPalindrome("racecar");
		assertTrue("Value provided failed palindrome validation",test);
		
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean test=Palindrome.isPalindrome("notPalindrome");
		assertFalse( "Test not created.",test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test=Palindrome.isPalindrome("Taco cat");
		assertTrue( "Failed",test );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test=Palindrome.isPalindrome("race on car");
		assertFalse( "Fails",test );
	}	
	
}
